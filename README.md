# Cifar-10 classification task using Horovod framework
The Cifar-10 classification problem is solved using TensorFlow and Horovod for distributed learning.
This work is a kind of Proof-of-Concept implementation of distributed learning rather than a try to achieve SoA performance.

## Network architecture

Network consists of a few Convolutional layers with MaxPooling layers, followed by
Fully Connected layer and final Fully Connected layer with NUM_CALSSES (10) outputs. Activation function ReLu is used.
- Convolutional layer, number of filters 32, filter size 3x3

- MaxPooling layer, filter size 2x2

- Convolutional layer, number of filters 64, filter size 3x3

- MaxPooling layer, filter size 2x2

- Convolutional layer, number of filters 128, filter size 3x3

- MaxPooling layer, filter size 2x2

- FC layer, number of neurons 128 

- FC layer, number of neurons 10 (NUM_CLASSES)

Adam optimizer with default parameters (except learning rate) is used, loss function - categorical cross entropy.

## Script files

Model is defined in ```model_def.py```
Model training is performed in ```cifar10.py```
To run training in a single process:
```python3 cifar10.py```

## Performance
Typical accuracy achieved after 10 epochs is approx. 70%
Typical output on a quite old laptop:
```Epoch 1/10```

```500/500 [==============================] - 65s 131ms/step - loss: 1.5997 - accuracy: 0.4148 - val_loss: 1.2420 - val_accuracy: 0.5497```

```Epoch 2/10```

```500/500 [==============================] - 72s 144ms/step - loss: 1.2217 - accuracy: 0.5633 - val_loss: 1.0994 - val_accuracy: 0.6133```

```Epoch 3/10```

```500/500 [==============================] - 74s 147ms/step - loss: 1.0607 - accuracy: 0.6268 - val_loss: 1.0362 - val_accuracy: 0.6344```

```Epoch 4/10```

```500/500 [==============================] - 74s 149ms/step - loss: 0.9551 - accuracy: 0.6647 - val_loss: 0.9868 - val_accuracy: 0.6501```

```Epoch 5/10```

```500/500 [==============================] - 78s 156ms/step - loss: 0.8889 - accuracy: 0.6852 - val_loss: 0.9345 - val_accuracy: 0.6702```

```Epoch 6/10```

```500/500 [==============================] - 76s 152ms/step - loss: 0.8216 - accuracy: 0.7102 - val_loss: 0.9306 - val_accuracy: 0.6797```

```Epoch 7/10```

```500/500 [==============================] - 76s 152ms/step - loss: 0.7712 - accuracy: 0.7279 - val_loss: 0.8670 - val_accuracy: 0.7021```

```Epoch 8/10```

```500/500 [==============================] - 77s 153ms/step - loss: 0.7256 - accuracy: 0.7416 - val_loss: 0.8687 - val_accuracy: 0.7017```

```Epoch 9/10```

```500/500 [==============================] - 76s 152ms/step - loss: 0.6842 - accuracy: 0.7549 - val_loss: 0.8814 - val_accuracy: 0.7046```

```Epoch 10/10```

```500/500 [==============================] - 77s 153ms/step - loss: 0.6459 - accuracy: 0.7699 - val_loss: 0.8629 - val_accuracy: 0.7104```

```Test loss: 0.8628686666488647```

```Test accuracy: 0.7103999853134155```

## Configuration

Since the purpose of this task is demonstration of distributed learning the configurable parameters are limited to

+ ```--epochs``` - number of epochs, default 3

+ ```--learning_rate``` - learning rate, default 0.002

+ ```--batch_size``` - batch size, default 100

## Distributed learning
For the distributed learning the Horovod framework is used. A few changes needed to be made for single-worker case are listed below:

- Import horovod: ```import horovod.tensorflow.keras as hvd``` 

- Init horovod: ``` hvd.init()```

- Adjust learning rate ``` learning_rate * hvd.size()```

- Add to model Horovod Distributed Optimizer ```opt = hvd.DistributedOptimizer(opt)```

- Broadcast initial variable states from rank 0 to all other processes. This is necessary to ensure consistent initialization  of all workers when training is started with random weights or restored from a checkpoint. ``` callbacks = [  hvd.callbacks.BroadcastGlobalVariablesCallback(0) ]```

- Save checkpoints only on worker 0 to prevent other workers from corrupting them.

    ```if hvd.rank() == 0:```
    
    ```    callbacks.append(tf.keras.callbacks.ModelCheckpoint('./checkpoint-{epoch}.h5'))```
    
## How to run
 Important note: Docker image includes TF 2.1, so it needs to be run on a CPU that includes AVX instructions (https://github.com/tensorflow/tensorflow/issues/18322)
 
 + Download Docker image

```docker pull evefim/cifar10:v3```

Docker image is built on the basis of 
```horovod/horovod:0.19.3-tf2.1.0-torch1.4.0-mxnet1.6.0-py3.6-cpu``` Docker image to avoid hassle with installation of libraries. In this setup everything works out of the box.

 + Run docker image

```docker run -it --privileged evefim/cifar10:v3 bash```

```--privileged``` option is needed to avoid Horovod related errors during training (known issue).
After start you will be directed to ```/federated_learning_challenge``` folder, where all scripts and this README are located.

 + Run ```horovodrun``` with N workers to start training
 
```horovodrun -np <N> python3 cifar10.py --epochs <ne> --learning_rate <lr> --batch_size <bs>```

or simply

```horovodrun -np <N> python3 cifar10.py``` 

for default parameters.


  