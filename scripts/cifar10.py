import argparse
import tensorflow as tf
import json
import os
import tensorflow.keras.backend as K
import horovod.tensorflow.keras as hvd

from tensorflow.keras import datasets
from model_def import build_model

def parseCommandLine():
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs',        type=int,   default=3)
    parser.add_argument('--learning-rate', type=float, default=0.002)
    parser.add_argument('--batch-size',    type=int,   default=100)
    args = parser.parse_args()

    return args

def main():
    args = parseCommandLine()
    batch_size = args.batch_size
    epochs = args.epochs
    learning_rate = args.learning_rate

    # Download dataset
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
      
    # Normalize pixel values to be between 0 and 1
    x_train, x_test = x_train / 255.0, x_test / 255.0
    class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer',
               'dog', 'frog', 'horse', 'ship', 'truck']

    # Horovod: init horovod
    hvd.init()
    size = hvd.size()
    if hvd.rank() == 0:
        print("Number of workers: ", size)

    # Horovod: adjust learning rate
    model = build_model(learning_rate * size)
    callbacks = [
    # Horovod: broadcast initial variable states from rank 0
    # to all other processes.
    # This is necessary to ensure consistent initialization
    # of all workers when training is started with random
    # weights or restored from a checkpoint.
    hvd.callbacks.BroadcastGlobalVariablesCallback(0),
    ]

    # Horovod: save checkpoints only on worker 0 to prevent
    # other workers from corrupting them.
    if hvd.rank() == 0:
        callbacks.append(tf.keras.callbacks.ModelCheckpoint('./checkpoint-{epoch}.h5'))
        
    history = model.fit(x_train, y_train,
                        epochs=epochs,
                        callbacks = callbacks,
                        batch_size = batch_size,
                        validation_data  = (x_test, y_test),
                        verbose = 1 if hvd.rank() == 0 else 0,
                        shuffle = True)
    
    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

if __name__ == "__main__":
    main()
